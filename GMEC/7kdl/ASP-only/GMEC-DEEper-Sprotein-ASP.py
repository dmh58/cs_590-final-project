  
import osprey

osprey.start(heapSizeMiB=10240)

#define a strand
strand = osprey.Strand('7kdl_mgl_h_no519.pdb')
strand.flexibility['A614'].setLibraryRotamers('ASP').setContinuous()
strand.flexibility['B614'].setLibraryRotamers('ASP').setContinuous()
strand.flexibility['C614'].setLibraryRotamers('ASP').setContinuous()

# strand = osprey.Strand('6vsb_noPP.pdb')
# strand.flexibility['A614'].setLibraryRotamers(osprey.WILD_TYPE).addWildTypeRotamers().setContinuous()
# strand.flexibility['B614'].setLibraryRotamers(osprey.WILD_TYPE).addWildTypeRotamers().setContinuous()
# strand.flexibility['C614'].setLibraryRotamers(osprey.WILD_TYPE).addWildTypeRotamers().setContinuous()


# make the conf space
bbflex = osprey.DEEPerStrandFlex(strand, '6vsb_noPP.d.pert', ['A614','B614','C614'], '7kdl_mgl_h_no519.pdb')
confSpace = osprey.ConfSpace([[strand,bbflex]])

# choose a forcefield
ffparams = osprey.ForcefieldParams()
ffparams.solvScale = 0.

# how should we compute energies of molecules?
ecalc = osprey.EnergyCalculator(confSpace, ffparams)

eref = osprey.ReferenceEnergies(confSpace, ecalc)

# how should we define energies of conformations?
confEcalc = osprey.ConfEnergyCalculator(confSpace, ecalc, referenceEnergies=eref)

# how should confs be ordered and searched?
emat = osprey.EnergyMatrix(confEcalc)

# find the best sequence and rotamers
gmec = osprey.DEEGMECFinder(emat, confSpace, ecalc, confEcalc, '6vsb_noPP.dee.pert', True, True).calcGMEC()