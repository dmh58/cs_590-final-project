
import osprey
osprey.start()
# INCREASE HEAP SPACE HERE osprey.start(heapSizeMiB=10240)

#should work now

# choose a forcefield
ffparams = osprey.ForcefieldParams()

# read a PDB file for molecular info
mol = osprey.readPdb('../pdb/7kdl_mgl_h_no519.pdb')

# make sure all strands share the same template library
templateLib = osprey.TemplateLibrary(ffparams.forcefld)

aas = ['ALA','ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS','MET','PHE','PRO','SER','THR','TRP','TYR','VAL']
pos = 'combined'

# define the protein strand
protein = osprey.Strand(mol, templateLib=templateLib, residues=['A612', 'A648'])
protein.flexibility['A612'].setLibraryRotamers(osprey.WILD_TYPE,'LYS','ARG').addWildTypeRotamers().setContinuous()
protein.flexibility['A613'].setLibraryRotamers(osprey.WILD_TYPE,'TRP','ILE').addWildTypeRotamers().setContinuous()
protein.flexibility['A614'].setLibraryRotamers(osprey.WILD_TYPE, 'ASP').addWildTypeRotamers().setContinuous() # LEU, ILE
protein.flexibility['A615'].setLibraryRotamers(osprey.WILD_TYPE,'GLU','ASP').addWildTypeRotamers().setContinuous()
# protein.flexibility['A616'].setLibraryRotamers(osprey.WILD_TYPE,'','').addWildTypeRotamers().setContinuous()
protein.flexibility['A647'].setLibraryRotamers(osprey.WILD_TYPE,'GLU','ASP').addWildTypeRotamers().setContinuous()
protein.flexibility['A648'].setLibraryRotamers(osprey.WILD_TYPE,'GLY').addWildTypeRotamers().setContinuous()

# define the ligand strand
ligand = osprey.Strand(mol, templateLib=templateLib, residues=['B700', 'B861'])
ligand.flexibility['B734'].setLibraryRotamers(osprey.WILD_TYPE,'ASP','GLU').addWildTypeRotamers().setContinuous()
ligand.flexibility['B735'].setLibraryRotamers(osprey.WILD_TYPE,'GLU','ASP').addWildTypeRotamers().setContinuous()
ligand.flexibility['B736'].setLibraryRotamers(osprey.WILD_TYPE,'ASP','GLU').addWildTypeRotamers().setContinuous()
ligand.flexibility['B857'].setLibraryRotamers(osprey.WILD_TYPE,'GLY').addWildTypeRotamers().setContinuous()
ligand.flexibility['B858'].setLibraryRotamers(osprey.WILD_TYPE,'ASP','GLU').addWildTypeRotamers().setContinuous()
ligand.flexibility['B860'].setLibraryRotamers(osprey.WILD_TYPE,'ASP','GLU').addWildTypeRotamers().setContinuous()
ligand.flexibility['B861'].setLibraryRotamers(osprey.WILD_TYPE,'GLU','TYR').addWildTypeRotamers().setContinuous()

# make the conf space for the protein
proteinConfSpace = osprey.ConfSpace(protein)

# make the conf space for the ligand
ligandConfSpace = osprey.ConfSpace(ligand)

# make the conf space for the protein+ligand complex
complexConfSpace = osprey.ConfSpace([protein, ligand])

# how should we compute energies of molecules?
# (give the complex conf space to the ecalc since it knows about all the templates and degrees of freedom)
parallelism = osprey.Parallelism(cpuCores=40)
ecalc = osprey.EnergyCalculator(complexConfSpace, ffparams, parallelism=parallelism)

# configure K*
kstar = osprey.KStar(
	proteinConfSpace,
	ligandConfSpace,
	complexConfSpace,
	epsilon=0.99, # you proabably want something more precise in your real designs
	writeSequencesToConsole=True,
	writeSequencesToFile= 'Results/'+ 'kstar' + pos + '.results.tsv'
)

# configure K* inputs for each conf space
for info in kstar.confSpaceInfos():

	# how should we define energies of conformations?
	eref = osprey.ReferenceEnergies(info.confSpace, ecalc)
	info.confEcalc = osprey.ConfEnergyCalculator(info.confSpace, ecalc, referenceEnergies=eref)

	# compute the energy matrix
	emat = osprey.EnergyMatrix(info.confEcalc, cacheFile='Results/'+ 'emat' + pos + '.%s.dat' % info.id)

	# how should we score each sequence?
	# (since we're in a loop, need capture variables above by using defaulted arguments)
	def makePfunc(rcs, confEcalc=info.confEcalc, emat=emat):
		return osprey.PartitionFunction(
			confEcalc,
			osprey.AStarTraditional(emat, rcs, showProgress=False),
			osprey.AStarTraditional(emat, rcs, showProgress=False),
			rcs
		)
	info.pfuncFactory = osprey.KStar.PfuncFactory(makePfunc)

# run K*
scoredSequences = kstar.run(ecalc.tasks)

# make a sequence analyzer to look at the results
analyzer = osprey.SequenceAnalyzer(kstar)

# use results
for scoredSequence in scoredSequences:
	print("result:")
	print("\tsequence: %s" % scoredSequence.sequence)
	print("\tK* score: %s" % scoredSequence.score)

	# write the sequence ensemble, with up to 10 of the lowest-energy conformations
	numConfs = 1
	analysis = analyzer.analyze(scoredSequence.sequence, numConfs)
	print(analysis)
	analysis.writePdb(
		'Results/'+ 'seq' + pos + '.%s.pdb' % scoredSequence.sequence,
		'Top %d conformations four sequence %s' % (numConfs, scoredSequence.sequence)
	)
