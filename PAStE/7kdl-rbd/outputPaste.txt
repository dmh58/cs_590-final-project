OSPREY 3.2.213, Python 2.7.17, Java 14.0.1, Linux-4.15.0-126-generic-x86_64-with-Ubuntu-18.04-bionic
Using up to 10240 MiB heap memory: 128 MiB for garbage, 10112 MiB for storage
read PDB file from file: 7kdl_mgl_h_no519.pdb
WARNING: 24 Strand residue(s) could not be matched to templates and were automatically deleted:
ASP A 405
GLU A 406
THR A 415
ASP A 420
ASP A 427
ASN A 440
ASP A 442
LYS A 458
GLU A 465
THR A 470
ASN A 501
TYR A 505
LEU A 518
LYS A 528
ASP A 568
ASP A 571
LEU A 582
GLU A 583
ASP A 586
ASN A 603
ASN A 616
GLU A 619
VAL A 620
ARG A 646
WARNING: 105 Strand residue(s) could not be matched to templates and were automatically deleted:
TYR B 380
VAL B 382
SER B 383
THR B 385
LYS B 386
LEU B 387
ASN B 388
ASP B 389
LEU B 390
PHE B 392
THR B 393
ASN B 394
VAL B 395
TYR B 396
ASP B 398
SER B 399
PHE B 400
VAL B 401
ILE B 402
ARG B 403
ASP B 405
GLU B 406
VAL B 407
ARG B 408
GLN B 409
ILE B 410
GLN B 414
THR B 415
LYS B 417
ILE B 418
ASP B 420
TYR B 421
ASN B 422
TYR B 423
LYS B 424
LEU B 425
ASP B 427
ASP B 428
PHE B 429
THR B 430
VAL B 433
ILE B 434
TRP B 436
ASN B 437
SER B 438
ASN B 439
ASN B 440
LEU B 441
ASP B 442
SER B 443
LYS B 444
VAL B 445
ASN B 448
TYR B 449
ASN B 450
TYR B 451
LEU B 452
TYR B 453
ARG B 454
LYS B 462
PHE B 464
GLU B 465
ARG B 466
LEU B 492
GLN B 493
SER B 494
TYR B 495
PHE B 497
GLN B 498
THR B 500
ASN B 501
VAL B 503
TYR B 505
GLN B 506
TYR B 508
ARG B 509
VAL B 510
VAL B 511
VAL B 512
LEU B 513
SER B 514
PHE B 515
THR B 523
VAL B 524
LYS B 528
LYS B 529
GLU B 554
GLU B 583
ASN B 603
ASN B 616
VAL B 620
ARG B 646
ASN B 657
GLU B 661
THR B 676
ASN B 709
ASN B 717
GLU B 748
LYS B 786
ILE B 794
ASP B 796
ASN B 801
LYS B 811
LYS B 814
THR B 827
Calculating reference energies for 7 residue confs...
Progress:  100.0%   ETA: 0 ns
Finished in 184.4 ms
Calculating energy matrix with 7 entries
Progress:  100.0%   ETA: 156.0 ms
Finished in 1.2 s
wrote energy matrix to file: /home/home3/ajb135/PAStE/rbd7kdl/emat.protein.dat
computing PAStE scores for 2 sequence(s) to epsilon = 0.99 ...
A614=gly   PAStE ddG: -0.000000 in [-0.000000,-0.000000]               Affect on Stability Unclear   
A614=ASP   PAStE ddG: 0.120632  in [0.120632 , 0.120632]              Mutation Decreases Stability   
