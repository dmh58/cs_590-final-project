import osprey
osprey.start(heapSizeMiB=10240)

# choose a forcefield
ffparams = osprey.ForcefieldParams()

# read a PDB file for molecular info
mol = osprey.readPdb('6vsb_nopp_h_no519.pdb')

# make sure all strands share the same template library
templateLib = osprey.TemplateLibrary(ffparams.forcefld)

# define the protein strand
protein = osprey.Strand(mol, templateLib=templateLib, residues=['A380', 'A648'])
protein.flexibility['A614'].setLibraryRotamers(osprey.WILD_TYPE, 'GLY').addWildTypeRotamers().setContinuous()

# define the ligand strand
ligand = osprey.Strand(mol, templateLib=templateLib, residues=['B380', 'B861'])


# make the conf space for the protein+ligand complex
complexConfSpace = osprey.ConfSpace([protein, ligand])

# how should we compute energies of molecules?
# (give the complex conf space to the ecalc since it knows about all the templates and degrees of freedom)
parallelism = osprey.Parallelism(cpuCores=40)
ecalc = osprey.EnergyCalculator(complexConfSpace, ffparams, parallelism=parallelism)

# configure PAStE
paste = osprey.Paste(
	complexConfSpace,
	numPDBs = 5,
	epsilon=0.99, # you proabably want something more precise in your real designs
	useWindowCriterion=True,
	writeSequencesToConsole=True,
	writeSequencesToFile='paste.results.tsv',
	mutFile='mut.txt'
)

# configure PAStE inputs for the conf space

# how should we define energies of conformations?
eref = osprey.ReferenceEnergies(paste.protein.confSpace, ecalc)
paste.protein.confEcalc = osprey.ConfEnergyCalculator(paste.protein.confSpace, ecalc, referenceEnergies=eref)

# compute the energy matrix
emat = osprey.EnergyMatrix(paste.protein.confEcalc, cacheFile='emat.%s.dat' % paste.protein.id)

# how should confs be ordered and searched? (don't forget to capture emat by using a defaulted argument)
def makeAStar(rcs, emat=emat):
	return osprey.AStarTraditional(emat, rcs, showProgress=False)
paste.protein.confSearchFactory = osprey.Paste.ConfSearchFactory(makeAStar)

# run PAStE
paste.run()