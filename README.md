# CS_590 Final Project

Final project for CS590 at Duke University, which explores binding and folding related energetic impacts of the D614G mutation in the SARS-CoV-2 S-protein, which has been proven to increase infectivity of the virus. This repo contains code and results pertinent to the following paper: https://duke.box.com/s/632o7ssiozwq6zgu216n7ud1ulvmnbuw

Code sourced from OSPREY3: https://github.com/donaldlab/OSPREY3

For questions, please contact: Alex Bajana (ajb135@duke.edu) or Dennis Harrsch (dmh58@duke.edu)
